<?php

$db_path = "/var/www/fcvnc/api/fcvnc.db"; //Path to the sqlite database - Please move out of the web directory
$remminaFullscreen = "/var/www/fcvnc/api/remmina_fullscreen.sh"; //Path to the script that makes Remmina full screem - Please move out of the web directory
$remminaProfile = "/var/fcvnc/Connection.remmina"; //Where to store Remmina Profile Configuration - Please do not store in the web directory
$remminapasswdPath = "/var/fcvnc/remminapasswd"; //Where is the remminapasswd binary - Please do not store in the web directory

$timeout_period = 15; //Period of time (in seconds) before sessions are automatically removed
$debugging = true; //Enable debugging mode?
$salt = "SpeedSchoolRocksMySocks"; //Salt for sessionkey - Change on every server FCVNC is deployed on

$XDISPLAY = ":0"; //X display to use when running Remmina and other graphical programs

//Instructor Console integration
$useFCVNCI = true; //Is there a FCVNCI server? Integrate with it? - You wouldn't integrate with a FCVNCI server if this server is "portable."
$fcvnciIP = "136.165.45.157"; //IP address of the FCVNCI server - Used for validating data coming from the instructor console

?>
