<?php
//Page to create user sessions

include(realpath(dirname(__FILE__)) . "/config.php"); //Pull in $db_path
//Maintenance script call
exec("php api/maintenance.php");

//Check if session already exists (both Cookie and in DB)
session_start();
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Session exists, forward to control page
			header('Location: ./control.php');
		}
		else {
			//Sessionkey not in DB, but has cookie. Delete cookie and force re-login
			session_destroy();
			session_start();
			session_destroy();
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Flipped Classroom TV Collaboration Student - <?php echo gethostname(); ?></title>
<!--[if lt IE 9]>
	<script src="./assets/javascripts/html5.js"></script>
<![endif]-->
	<link rel="stylesheet" href="./assets/stylesheets/style.css" />
<!--[if (gt IE 8) | (IEMobile)]><!-->
 	<link rel="stylesheet" href="./assets/stylesheets/unsemantic-grid-responsive.css" />
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
	<link rel="stylesheet" href="./assets/stylesheets/ie.css" />
<![endif]-->
</head>
<body>
  <div class="grid-container">
    <?php include('header.php'); ?>
    <div class="grid-100">
      <section class="login-block">
      	<form name="login" action="session.php" method="get">
		Name: <input type="text" name="user">
		<input type="submit" value="Enter">
	</form>
      </section>
    </div>
  </div>
<?php include("footer.html"); ?>
</body>
</html>
