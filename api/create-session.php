<?php
//Create a session for the user and record it in the database

include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path
//Maintenance script call
exec("php maintenance.php");

//Remove any exisiting PHP Session
session_start();
session_destroy();

session_start();
$_SESSION['user'] = htmlspecialchars($_GET['user']);
$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
$_SESSION['time'] = time();
$_SESSION['sessionkey'] = md5($salt . $_SESSION['user'] . time()  . $_SESSION['ip']);

try {
	$DBH = new PDO("sqlite:$db_path");
	if($debugging == true)
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

	//Check if sessionkey already exists in DB. If so, update time, else, create session.
	$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
	$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
	$query->execute();
	$row = $query->fetch(PDO::FETCH_NUM);
	if($row > 0) {
		//Sessionkey exists, update time
		$query = $DBH->query("UPDATE session SET time = " . $_SESSION['time'] . " WHERE id == " . $row[0]);
	}
	else {
		//Sessionkey does not exist, Create new session in DB
		$query = $DBH->prepare("INSERT INTO session (user, time, sessionkey, ip) VALUES (:user, :time, :sessionkey, :ip)");
		$data = array( 'user' => $_SESSION['user'], 'time' => $_SESSION['time'], 'sessionkey' => $_SESSION['sessionkey'], 'ip' => $_SESSION['ip'] );
		$query->execute($data);
	}
	$DBH = null;
}
catch(PDOException $e) {
	echo $e->getMessage();
}

?>
