<?php
//Returns username and userIDs of all users in DB
include(realpath(dirname(__FILE__)) . "/../../config.php");
//Maintenance script call
exec("php ../maintenance.php");

header('Content-Type: application/json');
if($useFCVNCI == true) {
        if($fcvnciIP == $_SERVER['REMOTE_ADDR']) {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging
		//Query user database
		$query = $DBH->query("SELECT id, user FROM session ORDER BY id ASC");
		$results = $query->fetchAll(PDO::FETCH_ASSOC);
                echo json_encode($results);
        }
        else {
                echo json_encode(array("Failed", "Requesting IP does not match config value!"));
        }
}
else {
        echo json_encode(array("Failed", "FCVNCI Integration Disabled!"));
}
?>
