<?php
//Returns hostname for FCVNCI collection
include(realpath(dirname(__FILE__)) . "/../../config.php");

header('Content-Type: application/json');
if($useFCVNCI == true) {
	if($fcvnciIP == $_SERVER['REMOTE_ADDR']) {
		echo json_encode(array("Success", gethostname()));
	}
	else {
		echo json_encode(array("Failed", "Requesting IP does not match config value!"));
	}
}
else {
	echo json_encode(array("Failed", "FCVNCI Integration Disabled!"));
}
?>
