<?php
//Stop displaying user screen on monitor
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path

header('Content-Type: application/json');

session_start();
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
                        $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

                //Check if sessionkey already exists and is presenting, if so kill remmina and set flag to 0
                $query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey AND presenting = 1");
                $query->bindParam(':sessionkey', $_SESSION['sessionkey']);
                $query->execute();
                $row = $query->fetch(PDO::FETCH_NUM);
                if($row > 0) {
                        //Change user presenting flags in DB to 0
                        $query = $DBH->prepare("UPDATE session SET presenting = 0 WHERE sessionkey = :sessionkey");
			$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
                        $query->execute();
                        //Kill remmina session
                        exec("pkill remmina");
                        echo json_encode("Success");
                }
                else {
			echo json_encode("Failed: Either you are not in the session or you are not presenting");
		}

	}
	catch(PDOException $e) {
		echo json_encode($e->getMessage());
	}
}

?>
