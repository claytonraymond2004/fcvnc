<?php
//Display the screen of a user on the server monitor
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path

header('Content-Type: application/json');
//Check if in session, if so create remmina profile and display on screen
session_start();
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

		//Check if sessionkey already exists in DB. If so, create Remmina Profile and show on monitor
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Change all presenting flags in DB to 0
			$query = $DBH->prepare("UPDATE session SET presenting = 0");
			$query->execute();
			//Change presenting flag in DB
			$query = $DBH->prepare("UPDATE session SET presenting = 1 WHERE sessionkey = :sessionkey");
			$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
			$query->execute();
			//If  vncsource = 1, create Remmina Profile based off localvncpassword and localvncport
			if($row[6] == 1) {
				$password = exec($remminapasswdPath . ' "' . $row[7] . '"');
				writeRemminaProfileFile($row[1], $row[4] . ":" . $row[8], $password, $remminaProfile);
			}
			//If vncsource = 0, create Remmina Profile based off sessionkey
			else {
				$password = exec($remminapasswdPath . ' "' . $row[3] . '"');
				writeRemminaProfileFile($row[1], $row[4], $password, $remminaProfile);
			}
			//Kill any already running remmina sessions and Start session on monitor
			exec("pkill remmina");
			exec("DISPLAY=" . $XDISPLAY . " remmina -c " . $remminaProfile . " > /dev/null &");
			//Use xdotool to full screen Remmina
			exec($remminaFullscreen . ' "' . $XDISPLAY . '" "' . $row[1] . '" > /dev/null &');
			echo 'Success';
		}
		else {
			echo 'Failed';
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
else {
	echo 'Failed';
}

function writeRemminaProfileFile($name, $ip, $password, $saveTo) {
	$outputFileContents = "";
	$outputFileContents .= "[remmina]\n";
	$outputFileContents .= "keymap=Map Meta Keys\n";
	$outputFileContents .= "ssh_auth=0\n";
	$outputFileContents .= "quality=2\n"; //Best=9, Good=2, Medium=1, 0=Poor
	$outputFileContents .= "disableencryption=1\n";
	$outputFileContents .= "ssh_charset=\n";
	$outputFileContents .= "ssh_privatekey=\n";
	$outputFileContents .= "colordepth=16\n";
	$outputFileContents .= "hscale=0\n";
	$outputFileContents .= "group=\n";
	$outputFileContents .= "password=$password\n";
	$outputFileContents .= "name=$name\n";
	$outputFileContents .= "ssh_loopback=0\n";
	$outputFileContents .= "viewonly=1\n";
	$outputFileContents .= "ssh_username=\n";
	$outputFileContents .= "ssh_server=\n";
	$outputFileContents .= "disableclipboard=1\n";
	$outputFileContents .= "protocol=VNC\n";
	$outputFileContents .= "vscale=0\n";
	$outputFileContents .= "ssh_enabled=0\n";
	$outputFileContents .= "username=\n";
	$outputFileContents .= "showcursor=1\n";
	$outputFileContents .= "disableserverinput=0\n";
	$outputFileContents .= "server=$ip\n";
	$outputFileContents .= "aspectscale=1\n";
	$outputFileContents .= "scale=1\n";
	$outputFileContents .= "window_maximize=1\n";
	$outputFileContents .= "viewmode=1";

	file_put_contents($saveTo, $outputFileContents);
}

?>
