#!/bin/bash

# Fullscreen Remmina via xdotool and keyboard shortcuts
# Takes a DISPLAY ($1) and Window Title ($2) as arguments

DISPLAY=$1
TITLE=$2

# Export display variable
export DISPLAY=$DISPLAY

# Check if Window already exists
WINDOWID=`xdotool search --name "^$TITLE$" | head -1`

# Loop until window with TITLE found
while [ -z $WINDOWID ]
do
	sleep .5
	WINDOWID=`xdotool search --name "^$TITLE$" | head -1`
done

# Make found window focused
$(xdotool windowactivate $WINDOWID)

# Send Control_R+F to window
$(xdotool key Control_R+F)
