<?php
//Keepalive called by clients. Update time in DB and session. Return list of users and active presenter (get-status.php).
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $db_path
//Maintenance script call
exec("php maintenance.php");

header('Content-Type: application/json');
//Update time of DB entry for session
session_start();
if(isset($_SESSION['sessionkey'])) {
	$_SESSION['time'] = time();
	try {
		$DBH = new PDO("sqlite:$db_path");
		if($debugging == true)
			$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

		//Check if sessionkey already exists in DB. If so, update time, else, delete cookie session
		$query = $DBH->prepare("SELECT * FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$row = $query->fetch(PDO::FETCH_NUM);
		if($row > 0) {
			//Sessionkey exists, update time and return status JSON
			$query = $DBH->query("UPDATE session SET time = " . $_SESSION['time'] . " WHERE id == " . $row[0]);

			//Get all sessions in DB
			$query = $DBH->query("SELECT user, presenting FROM session");
			$query->setFetchMode(PDO::FETCH_ASSOC);
			//Sessions exist, return JSON of user and presenting status (0=Not presenting, 1=Presenting)
			$userlist = array();
			while($row = $query->fetch()) {
				$userlist[] = $row;
			}
			echo json_encode(array("users" => $userlist));
		}
		else {
			session_destroy();
			echo '{"users":[]}';
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
else {
	echo '{"users":[]}';
}
?>
