<?php
header('Content-Type: application/json');
//Only allow one instance of this script to run
$fp = fopen('/tmp/fcvnc-maintenance.lock', 'c+');
if(!flock($fp, LOCK_EX | LOCK_NB)) {
	die(json_encode(array("Failed", "Script already running! Only 1 instance allowed.")));
}

//Cleans up DB removing entries that are too old
include(realpath(dirname(__FILE__)) . "/../config.php"); //Pull in $timeout_period and $db_path

//Connect to DB and find entires where time difference is > $timeout_period
try {
	$DBH = new PDO("sqlite:$db_path");
	if($debugging == true)
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging

	//Check if user that is going to be removed is currently presenting, if so, kill Remmina
	$query = $DBH->prepare("SELECT * FROM session WHERE presenting = 1 AND (" . time() . "-time) > $timeout_period");
	$query->execute();
	$row = $query->fetch(PDO::FETCH_NUM);
	if($row > 0) {
		exec("pkill remmina");
	}

	//Remove users who's time field is out of the timeout_period
	$DBH->exec("DELETE FROM session WHERE (" . time() . "-time) > $timeout_period");
	$DBH = null;
}
catch(PDOException $e) {
	echo $e->getMessage();
}

//Close file lock
fclose($fp);
?>
