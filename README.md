# FCVNC - Flipped Classroom VNC TV Collaboration
This project is to be hosted on a computer connected to a large monitor (TV) to allow collaboration. This package provides a location where users may connect and push the content on their screen to the TV to share it with other users.

It is designed to be used in a classroom environment, where there are approximately 3 students gathered around one TV. It also provides the ability to for an instructor user to broadcast their display (or select a specific student's display) to specific (or all) TVs in the classroom.

The project relies on student computers running a VNC server (a Java applet VNC server is provided if necessary) and the server this package is running on to have a VNC client installed. The project provides a website that allows students to direct the server to connect to their VNC server and display it on the TV.

Required Hardware:

* Monitors for student stations
* Small computers to connect to each monitor - We are using a [Zotac ZBOX](http://www.amazon.com/Zotac-Dual-Core-Barebone-Memory-ZBOX-ID85-PLUS-U/dp/B00CFFADN2) - Recommended to be powerful enough to playback HD video for future enhancements.
* Isolated LAN - Prevents users from gaining access to the system outside of the classroom.

Required Software: 

* Apache + PHP + PDO + SQLite
* XServer
* Remmina - VNC Viewer for Linux
* xdotool - For sending the Remmina Full Screen shortcut
* [remminapasswd](http://sourceforge.net/apps/phpbb/remmina/viewtopic.php?f=1&t=43)
    * Must be recompiled on every computer this is deployed on.
        * Download the [Remmina source](https://github.com/FreeRDP/Remmina).
        * Install the packages: `pkg-config libglib2.0-dev libgtk2.0-dev libgcrypt-dev`
        * Extract from Remmina souce:  

                remmina/src/remmina_crypt.c
		        remmina/src/remmina_crypt.h
		        remmina/src/remmina_pref.c
		        remmina/src/remmina_pref.h
        		remmina/src/remmina_string_array.c
        		remmina/src/remmina_string_array.h

        * Create an empty `config.h` file in the directory where the above files are `touch config.h`.
	    * Create a remminapasswd.c file with the code in the remminapasswd forum post above, editing the include lines to include the "_" in the file names above.
        * Run:

                gcc -DHAVE_LIBGCRYPT -o remminapasswd remminapasswd.c remmina_crypt.c remmina_pref.c remmina_string_array.c -Wall `pkg-config --libs --cflags gtk+-2.0` `libgcrypt-config --cflags --libs`
            
        * Give remminapasswd execute permissions `chmod +x remminapasswd`.


Note: Project devloped and tested on Debian 7 LXDE


Tools Used:

* JQuery
* Unsemantic CSS Framework
* [freegsvncj.jar](http://gotoservers.com/gsvncj.html)

  
*** For PHP to launch Remmina, the user logged in and running the X server must be the same user as running Apache (www-data on debian) ***  
To do this:  
Give the "www-data" user a password (as root) `passwd www-data`.  
Log into the computer as the "www-data" user


*** /var/fcvnc/ ***  
I set up a `/var/fcvnc` folder with correct permissions for the storage of private data.  
Make sure the `config.php` points to the correct path when doing this and that the permissions are correct.  
I also changed the "www-data" home folder to this directory to avoid cluttering up the `/var/www` directory (edit in `/etc/passwd`)