<?php
//Main page for users. Contains list of users, current server status, and the VNC Server/Server Settigns

include(realpath(dirname(__FILE__)) . "/config.php"); //Pull in $db_path
//Maintenance script call
exec("php api/maintenance.php");

//Check if session exists (both Cookie and in DB). Redirect to login if session doesn't exist.
session_start();
if(isset($_SESSION['sessionkey'])) {
	try {
		$DBH = new PDO("sqlite:$db_path");
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); //Debugging
		$query = $DBH->prepare("SELECT COUNT(id) FROM session WHERE sessionkey = :sessionkey");
		$query->bindParam(':sessionkey', $_SESSION['sessionkey']);
		$query->execute();
		$rows = $query->fetch(PDO::FETCH_NUM);
		if($rows['0'] == 0) {
			//Sessionkey not in DB, but has cookie. Delete cookie and force re-login
			session_destroy();
			header('Location: ./index.php');
		}
		$DBH = null;
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}
}
else {
	session_destroy();
	header('Location: ./index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Flipped Classroom TV Collaboration Student - <?php echo gethostname(); ?></title>
<!--[if lt IE 9]>
	<script src="./assets/javascripts/html5.js"></script>
<![endif]-->
	<link rel="stylesheet" href="./assets/stylesheets/style.css" />
<!--[if (gt IE 8) | (IEMobile)]><!-->
	<link rel="stylesheet" href="./assets/stylesheets/unsemantic-grid-responsive.css" />
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
	<link rel="stylesheet" href="./assets/stylesheets/ie.css" />
<![endif]-->
<script type="text/javascript" src="assets/javascripts/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="assets/javascripts/control.js"></script>
</head>
<body>
  <div class="grid-container">
    <?php include('header.php'); ?>
    	<div class="grid-33 mobile-grid-100">
    		<section class="user-list-block">
				<h3>Users</h3>
				<div class="user-list-content">
					<div class="user-list"></div><br />
					<button type="button" style="width:100%;" onclick="logout()">Logout</button>
				</div>
      		</section>
    	</div>
    <div class="grid-33 mobile-grid-100">
    	<section class="currently-presenting-block">
			<h3>Presenter Status</h3>
			<div class="server-status-content">
				<div class="presenting-user"></div><br />
				<button type="button" style="width:49%;" onclick="present()">Present</button>
				<button type="button" style="width:49%; float:right;" onclick="stopPresent()">Stop Presenting</button>
			</div>
    	</section>
    </div>
    <div class="grid-33 mobile-grid-100">
      <section class="control-block">
	<h3>VNC Server</h3>
	<div id="tabContainer">
		<div class="tabs">
			<ul>
				<li id="tabHeader_1">Java VNC Server (Slower)</li>
				<li id="tabHeader_2">Local VNC Server (Faster)</li>
			</ul>
		</div>
		<div class="tabscontent">
			<div class="tabpage" id="tabpage_1">
			<center>Please do not change the port or password below.</center>
				<applet code="GSVNCJ.class" archive="assets/freegsvncj.jar" width=350 height=250> 
					<param name="port" value="5900"> 
					<param name="password" value="<?php echo $_SESSION['sessionkey']; ?>"> 
					<param name="autoStart" value="true"> 
					<param name="titleLabel" value="Java VNC Server"> 
					<param name="portLabel" value="Port"> 
					<param name="passwordLabel" value="Password"> 
					<param name="startLabel" value="Enable"> 
					<param name="stopLabel" value="Disable"> 
					<param name="mainBackground" value="CCCCCC"> 
					<param name="mainForeground" value="000"> 
					<param name="buttonBackground" value="8A2529"> 
					<param name="buttonForeground" value="FFFFFF"> 
					<param name="msgBackground" value="CCCCCC"> 
					<param name="msgForeground" value="000000"> 
					<param name="MSG1" value="Starting authenication {0}..."> 
					<param name="MSG2" value="{0} authentication successfull!"> 
					<param name="MSG3" value="{0} authentication failed!"> 
					<param name="MSG4" value="{0} can''t be opened!"> 
					<param name="MSG5" value="Exception: {0}"> 
					<param name="MSG6" value="Start listening on {0}..."> 
					<param name="MSG7" value="Client address: {0}"> 
					<param name="MSG8" value="shutting down {0} server VNCServer"> 
					<param name="MSG9" value="Client {0} closed"> 
				</applet>
			</div>
			<div class="tabpage" id="tabpage_2">
				<table border="0" cellpadding="1" cellspacing="1" style="width: 100%;">
					<tbody>
						<tr>
							<td style="width: 65px;">Port:</td><td><input type="number" id="vncport" name="port" min="1" max="65535" value="5900" style="width: 100%"></td>
						</tr>
						<tr>
							<td style="width: 65px;">Password:</td><td><input type="password" id="vncpassword" name="vncpassword" style="width: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center;"><input type="button" value="Save" style="width:100%;" onclick="saveLocalVNCSettings('#vncport', '#vncpassword')"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
      </section>
    </div>
    </div>
  </div>
<?php include("footer.html"); ?>
</body>
</html>
