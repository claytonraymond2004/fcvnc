window.setInterval(function(){updateUsers(".user-list", ".presenting-user")}, 1000);

function logout() {
	$.getJSON("api/destroy-session.php", function(data) {
		if(data == "Success") {
			window.location.href = "index.php";
		}
		else {
			alert("Failed to log out...");
		}
	});
}

function saveLocalVNCSettings(portField, passwordField) {
	$.ajax({
		type: "POST",
		url: "api/set-vnc-settings.php",
		data: { port: $(portField).val(), password: $(passwordField).val()}
	});
}

function present() {
	$.getJSON("api/start-present.php", function(data) {
		if(data != "Success") {
			alert("Failed to present: " + data);
		}
	});
}

function stopPresent() {
	$.getJSON("api/stop-present.php", function(data) {
		if(data != "Success") {
			alert("Failed to stop presenting: " + data);		}
	});
}

function updateUsers(userlist, presentinguser) {
	//$(".user-list").load("api/heartbeat.php");
	$.getJSON("api/heartbeat.php", function(data) {
		var outputhtml = "";
		var presenterDetected = false;
		if(data.users.length == 0) {
			window.location.href = "index.php";
		}
		for(var i = 0; i < data.users.length; i++) {
			var user = data.users[i];
			outputhtml += user.user + "</br>";
			if(user.presenting == 1) {
				$(presentinguser).html(user.user);
				presenterDetected = true;
			}
		}
		$(userlist).html(outputhtml);
		if(presenterDetected == false) {
			$(presentinguser).html("Nobody Presenting");
		}
	});
}

//Tabs
window.onload=function() {
	// get tab container
	var container = document.getElementById("tabContainer");
	// set current tab
	var navitem = container.querySelector(".tabs ul li");
	//store which tab we are on
	var ident = navitem.id.split("_")[1];
	navitem.parentNode.setAttribute("data-current",ident);
	//set current tab with class of activetabheader
	navitem.setAttribute("class","tabActiveHeader");
	//hide two tab contents we don't need
	var pages = container.querySelectorAll(".tabpage");
	for (var i = 1; i < pages.length; i++) {
		pages[i].style.display="none";
	}
	//this adds click event to tabs
	var tabs = container.querySelectorAll(".tabs ul li");
	for (var i = 0; i < tabs.length; i++) {
	tabs[i].onclick=displayPage;
	}
}
// on click of one of tabs
function displayPage() {
	var current = this.parentNode.getAttribute("data-current");
	//remove class of activetabheader and hide old contents
	document.getElementById("tabHeader_" + current).removeAttribute("class");
	document.getElementById("tabpage_" + current).style.display="none";

	var ident = this.id.split("_")[1];
	//add class of activetabheader to new active tab and show contents
	this.setAttribute("class","tabActiveHeader");
	document.getElementById("tabpage_" + ident).style.display="block";
	this.parentNode.setAttribute("data-current",ident);
	//Tab is JAVA VNC server
	if(ident == 1) {
		$.ajax({
			type: "POST",
			url: "api/set-vnc-server.php",
			data: { vncsource: "0"}
		});
	}
	//Tab is local VNC server
	if(ident == 2) {
		$.ajax({
			type: "POST",
			url: "api/set-vnc-server.php",
			data: { vncsource: "1"}
		});
	}
}